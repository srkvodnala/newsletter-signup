const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const https = require("https");

const app = express();
app.use(express.static("public"))

app.use(bodyParser.urlencoded({ extended: true }));


app.get("/", (req, res) => {
    res.sendFile(__dirname + "/signup.html")
})

app.post("/", (req, res) => {
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const email = req.body.email

    // This is js object contains key:value pairs where keys should be identified by mailchimp.
    const data = {
        // members are array of objects here we have only one subscriber at a time. So only onew object 
        members: [{
            email_address: email,
            status: "subscribed",
            merge_fields: {
                FNAME: firstName,
                LNAME: lastName
            }
        }]
    }
    const jsonData = JSON.stringify(data);

    const url = "https://us20.api.mailchimp.com/3.0/lists/58660786c6"
    const options = {
        method: "POST",
        auth: "srk:30421dbd04259a4dafd7d800bc7025d6-us20"
    }

    const request = https.request(url, options, (response) => {
            // console.log("==> Errors")
            //     //console.log((response))
            // console.log(" Errors <==")


            // if (response.statusCode === 200) {
            //     res.sendFile(__dirname + "/success.html");
            // } else {
            //     res.sendFile(__dirname + "/failure.html");
            // }

            response.on("data", (data) => {
                console.log("====> HTTPS REQUEST DATA START <====")
                console.log(JSON.parse(data).errors[0] == null)
                if (JSON.parse(data).errors[0] == null) {
                    res.sendFile(__dirname + "/success.html");
                } else {
                    res.sendFile(__dirname + "/failure.html");
                }
                console.log("====> HTTPS REQUEST DATA END <====")
            })
        })
        // Till now we made request but we did not specified what to send to mailchimp

    // to send send save request in a const variable (Line 41)
    // Use const request to send data to mailchimp server with request.write method

    console.log(jsonData)
    request.write(jsonData);
    request.end();
})

app.post("/failure", (req, res) => {
    res.redirect("/")
})

app.listen(3000, () => console.log("Server running on port 3000"))